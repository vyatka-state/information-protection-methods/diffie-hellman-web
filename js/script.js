
const p = 997;
const g = 7;
let myPrivateKey;
let myPublicKey;
let pressedButton;


document.getElementById('diffieHellmanForm').onsubmit = (event) => {
    const outputTextArea = document.getElementById('outputText');
    const inputText = document.getElementById('inputText').value;
    const sharedSecret = generateSharedSecret(myPrivateKey, document.getElementById('theirPublicKey').value, p);

    if (pressedButton === 'encryptButton') {
        outputTextArea.value = encrypt(inputText, sharedSecret);
    } else if (pressedButton === 'decryptButton') {
        try {
            outputTextArea.value = decrypt(inputText.split(',').map(Number), sharedSecret);
        } catch (e) {
            alert('Зашифрованный текст должен быть введён в формате "число,число,число,...". Например: 1055,1088,1080,1074,1077,1090');
        }
    } else {
        alert('Упс... Что-то пошло не так');
    }

    pressedButton = null;
    event.preventDefault();
}

document.getElementById('encryptButton').onclick = () => {
    pressedButton = 'encryptButton';
}

document.getElementById('decryptButton').onclick = () => {
    pressedButton = 'decryptButton';
}

onload = () => {
    myPrivateKey = generatePrivateKey(p);
    myPublicKey = generatePublicKey(p, g, myPrivateKey);
    document.getElementById('myPublicKey').value = myPublicKey;
}


generatePrivateKey = (p) => {
    return Math.floor(Math.random() * 0.5 * p + 0.25 * p);
}

generatePublicKey = (p, g, privateKey) => {
    return Number((BigInt(g) ** BigInt(privateKey)) % BigInt(p));
}

generateSharedSecret = (myPrivateKey, theirPublicKey, p) => {
    return Number((BigInt(theirPublicKey) ** BigInt(myPrivateKey)) % BigInt(p));
}

encrypt = (message, sharedSecret) => {
    const encryptedMessage = [];
    for (let i = 0; i < message.length; ++i) {
        encryptedMessage.push(message.charCodeAt(i) ^ sharedSecret)
    }
    return encryptedMessage;
}

isNumeric = (value) => {
    return /^-?\d+$/.test(value.toString());
}

decrypt = (encryptedMessage, sharedSecret) => {
    const decryptedMessage = [];
    for (let i = 0; i < encryptedMessage.length; ++i) {
        if (!isNumeric(encryptedMessage[i])) {
            throw new Error('В массиве должны быть только числа');
        }
        decryptedMessage.push(String.fromCharCode(encryptedMessage[i] ^ sharedSecret));
    }
    return decryptedMessage.join('');
}
