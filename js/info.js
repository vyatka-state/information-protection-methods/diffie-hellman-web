document.getElementById('info').onclick = () => {
    document.body.insertAdjacentHTML('afterbegin', `
        <div id="curtain">
          <div id="popup" style="width: 550px; padding-right: 20px; height: 700px;">
            <div class="content">
              <h2 class="header">Информация о программе</h2>
              <div class="message" style="text-align: start; margin-left: 20px;">
                <div>
                    Протокол Ди́ффи — Хе́ллмана (англ. Diffie–Hellman key exchange protocol, DH) — криптографический протокол, позволяющий двум и более сторонам получить общий секретный ключ, используя незащищенный от прослушивания канал связи. Полученный ключ используется для шифрования дальнейшего обмена с помощью алгоритмов симметричного шифрования.
                </div>
                <h3 class="header">Инструкция по использованию</h3>
                <h4 class="header">Шифрование сообщения</h4>
                <ol>
                  <li>Автоматически будет создан приватный и публичный ключ (публичный будет показан в интерфейсе)</li>
                  <li>Ввести публичный ключ собеседника в соответствующее поле</li>
                  <li>Ввести сообщение, которое нужно зашифровать</li>
                  <li>Нажать на кнопку "Расшифровать"</li>
                  <li>Зашифрованное сообщение будет выведено ниже</li>
                </ol>
                <h4 class="header">Расшифровка сообщения</h4>
                <ol>
                  <li>Автоматически будет создан приватный и публичный ключ (публичный будет показан в интерфейсе)</li>
                  <li>Ввести публичный ключ собеседника в соответствующее поле</li>
                  <li>Ввести сообщение, которое нужно расшифровать</li>
                  <li>Нажать на кнопку "Расшифровать"</li>
                  <li>Расшифрованное сообщение будет выведено ниже</li>
                </ol>
              </div>
              <div>
                <button id="confirm-btn" class="button">ЗДОРОВО</button>
              </div>
            </>
          </ul>
        </div>
  `);

    document.getElementById('confirm-btn').onclick = () => {
        document.getElementById('popup').remove();
        document.getElementById('curtain').remove();
    };
}